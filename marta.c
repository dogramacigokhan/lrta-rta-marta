#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "marta.h"

void solve(int from[N][N], int to[N][N], int agentCount, enum Algorithm algorithm)
{
	srand(time(NULL));

	State *state = malloc(sizeof(State));
	initState(state, from, to, NULL, &globalVisitedNodeCount, &globalVisitedNodes, &globalVisitedNodeHeuristics);

	switch (algorithm) {
	case LRTA:
		lrta(state, to);
		break;
	case RTA:
		rta(state, to);
		break;
	case MARTA:
		marta(from, to, agentCount);
		break;
	}

	freeState(state);
}

void lrta(State *state, int to[N][N])
{
	/*
	 * LRTA - Learning Real Time A* Algorithm:
	 * Require: x0 . initial state
	 * Require: h0() . initial heuristic values for each state
	 * 1: for all y � S do . initialize heuristic values
	 * 2:	h(y) <- h0(y)
	 * 3: end for
	 * 4: x <- x0 . x is the current state
	 * 5: while x not � G do
	 * 6:	h(x) <- min[y � Chd(x)](k(x; y) + h(y)) . Look ahead and update value
	 * 7: Move to a child y � Chd(x) where
	 * 8:	y � argmin[z � Chd(x)](k(x; z) + h(z)) . Action Execution
	 * 9:	If there are more than one such child state, choose among them arbitrarily (tie break).
	 * 10: end while
	 *
	 * S is the set of states.
	 * G is the set of goal states.
	 * h(x), heuristic function, is the estimated cost from state x to the nearest goal state.
	 * Chd(x) is the set of child states/successors of x
	 * k(x; y) is the cost of move from x to its child/successor y.
	 */

	if (!state) {
		return;
	}

	static int step;
	printf("Step:%d ", ++step);
	print(*state);
	addToVisited(state, &globalVisitedNodeCount, &globalVisitedNodes, &globalVisitedNodeHeuristics);

	if (isGoal(state->frame, to)) {
		return; // Solved, exit
	}

	setChildren(state, to, &globalVisitedNodeCount, &globalVisitedNodes,
		&globalVisitedNodeHeuristics); // Step 6: Expand current state
	State *bestChild = NULL;
	int minCost = INF;

	int i;
	for (i = 0; i < state->childCount; i++) {
		State *child = state->children[i]; // Step 6: Get child state (sy)
		if (isGoal(child->frame, to)) {
			return lrta(child, to); // Solved
		}

		int cost = 1 + child->heuristic; // Step 6: k(x; y) + h(y)
		if (cost < minCost) {
			bestChild = child;
			minCost = cost;
		}
	}

	setHeuristic(state, minCost, &globalVisitedNodeHeuristics); // Step 6: h(x) <- min[y � Chd(x)](k(x; y) + h(y))
	if (bestChild != NULL) {
		lrta(bestChild, to); // Step 8: Move to child with minimum cost
	}
}

void rta(State *state, int to[N][N])
{
	/*
	 * RTA - Real Time A* algorithm:
	 * 1: sx <- s0 . Initialization
	 * 2: Expand sx . Expansion
	 * 3: if g � Chd(sx) then Move to goal and stop
	 * 4: end if . Termination
	 * 5: for all sy � Chd(sx) do
	 * 6: f(sx; sy) = k(sx; sy) + f(sy) where . Look-ahead search
	 *		f(sy) = min [(sw � W(sy;d))] (c(sy; sw) + h(sw))
	 * 7: end for
	 * 8: Choose the best child sz <- argmin[(sy � Chd(sx))]f(sx; sy). Ties broken randomly choice
	 * 9: h(sx) <- f(sx; sz') where sz' <- argmin [(sy � Chd(sx)-{sz})] f(sx; sy) is the estimated cost of
	 *		2nd best child. If there is no second best estimation, h(sx) <- infinite
	 * 10: sx <- sz . Move
	 * 11: Goto 2
	 *
	 * S is the set of states
	 * si is a state, i.e., si 2 S
	 * Chd(si) is the set of child states/successors of si
	 * d represents the look-ahead depth for the search
	 * W (sy; d): the set of leaves of look-ahead search tree of depth d
	 * c(sy; sw): actual cost from sy to sw
	 */

	if (!state) {
		return;
	}

	static int step;
	printf("Step:%d ", ++step);
	print(*state);
	addToVisited(state, &globalVisitedNodeCount, &globalVisitedNodes, &globalVisitedNodeHeuristics);

	if (isGoal(state->frame, to)) {
		return; // Solved, exit
	}

	setChildren(state, to, &globalVisitedNodeCount, &globalVisitedNodes,
		&globalVisitedNodeHeuristics); // Step 2: Expand current state
	State *bestChild = NULL; // Init variables to set during the f(sx; sy) calculation
	int minCost = INF,
		secondMinCost = INF;

	int i;
	for (i = 0; i < state->childCount; i++) {
		State *child = state->children[i]; // Step 5: Get child state (sy)
		if (isGoal(child->frame, to)) {
			return rta(child, to); // Solved
		}

		int cost = 1 + rta_f(child, to, &globalVisitedNodeCount, &globalVisitedNodes,
			&globalVisitedNodeHeuristics); // Step 6: f(sx; sy) = k(sx; sy) + f(sy) = 1 + f(sy)
		if (cost <= minCost) {
			bestChild = child; // Step 8
			secondMinCost = minCost;
			minCost = cost;
		}
		else if (cost < secondMinCost) {
			secondMinCost = cost;
		}
	}

	setHeuristic(state, secondMinCost, &globalVisitedNodeHeuristics); // Step 9
	if (bestChild != NULL) {
		rta(bestChild, to); // Step 10-11
	}
}

int rta_f(State *state, int to[N][N], int *visitedNodeCount,
	int(*visitedNodes)[CACHE_SIZE][N][N], int(*visitedNodeHeuristics)[CACHE_SIZE])
{
	setChildren(state, to, visitedNodeCount,
		visitedNodes, visitedNodeHeuristics); // Expand state
	int i, minCost = INF;

	for (i = 0; i < state->childCount; i++) {
		State *child = state->children[i]; // Grandchild node (sw)
		int cost = 1 + child->heuristic;	// c(sy; sw) + h(sw)
		if (cost < minCost) {
			minCost = cost; // f(sy) = min [(sw � W(sy;d))] (c(sy; sw) + h(sw))
		}
	}
	return minCost;
}

void marta(int from[N][N], int to[N][N], int agentCount)
{
	/*
	 * MARTA - Multi-agent Real Time A* Algorithm:
	 * RTA*: h(.) update (2nd best) -> hinders others from visiting the
	 * same state
	 * LRTA*: h(.) update (best) -> increases time as agents redundantly
	 * revisit the same states
	 * Two sorts of estimated costs
	 * hG(.) -> shared by all agents -> updated by LRTA* not to
	 * overestimate
	 * hL(.) -> local for each agent -> updated by RTA* to refrain from
	 * visiting the same state again
	 * Modify �Look-ahead Search� and �Estimation Update� steps of RTA* as follows:
	 * 1: for all sy � Chd(sx) do
	 * 2: f (sx; sy) = c(sx; sy) + h(sy) where . Look-ahead search
	 *
	 *		h(sy) = [hG(sy)	if sy has not been visited
	 *				[hL(sy)	if sy has been visited
	 * 3: end for
	 */

	if (agentCount <= 0) {
		return;
	}

	int visitedNodeCount[agentCount] = { 0 };
	int visitedNodes[agentCount][CACHE_SIZE][N][N] = { {{{0}}} };
	int visitedNodeHeuristics[agentCount][CACHE_SIZE] = { {0} };

	int i;
	State *states[agentCount];
	State *nextStates[agentCount];

	for (i = 0; i < agentCount; i++) {
		// Initialize states for each agent
		states[i] = malloc(sizeof(State));
		initState(states[i], from, to, NULL, &visitedNodeCount[i],
			&visitedNodes[i], &visitedNodeHeuristics[i]);
		nextStates[i] = states[i];
	}

	short goal;
	short goalAgentIndex;
	int step;
	for (goal = 0, step = 1; !goal; step++) {
		printf("Step: %d\n", step);

		for (i = 0; i < agentCount; i++) {
			State *newState = NULL;
			marta_search(nextStates[i], to, &goal, &visitedNodeCount[i],
				&visitedNodes[i], &visitedNodeHeuristics[i], &newState);
			nextStates[i] = newState;

			if (goal) {
				goalAgentIndex = i;
			}
			printf("Agent%d: %c\t", i + 1, newState->direction);
			print(*nextStates[i]);
		}
		printf("\n");
	}

	printf("Agent%d reaches the goal.\n", i);
	for (i = 0; i < agentCount; i++) {
		// Free states
		free(states[i]);
	}
}

void marta_search(State *state, int to[N][N], short *goal, int *visitedNodeCount,
	int(*visitedNodes)[CACHE_SIZE][N][N], int(*visitedNodeHeuristics)[CACHE_SIZE], State **newState)
{
	if (!state) {
		return;
	}

	addToVisited(state, visitedNodeCount, visitedNodes, visitedNodeHeuristics);

	if (isGoal(state->frame, to)) {
		*goal = 1;
		return; // Solved, exit
	}

	setChildren(state, to, visitedNodeCount, visitedNodes,
		visitedNodeHeuristics); // Expand current state
	State *bestChild = NULL; // Init variables to set during the f(sx; sy) calculation
	int minCost = INF,
		secondMinCost = INF;

	int i;
	for (i = 0; i < state->childCount; i++) {
		State *child = state->children[i]; // Get child state (sy)
		if (isGoal(child->frame, to)) {
			*goal = 1;
			*newState = child;
			return;	// Solved, exit
		}

		// Check if child state is visited globally
		int indexCache = child->arrayIndex;
		setVisitedArrayIndex(child, &globalVisitedNodeCount, &globalVisitedNodes);
		short childIsVisited = isVisited(child);
		child->arrayIndex = indexCache;

		int cost = 0;
		if (childIsVisited) {
			cost = 1 + rta_f(child, to, visitedNodeCount, visitedNodes,
				visitedNodeHeuristics); // cost + hL
		}
		else {
			addToVisited(child, &globalVisitedNodeCount, &globalVisitedNodes, &globalVisitedNodeHeuristics);
			cost = 1 + globalVisitedNodeHeuristics[child->arrayIndex]; // cost + hG
		}

		if (cost <= minCost) {
			bestChild = child;
			secondMinCost = minCost;
			minCost = cost;
		}
		else if (cost < secondMinCost) {
			secondMinCost = cost;
		}
	}

	// Update hG
	int indexCache = state->arrayIndex;
	setVisitedArrayIndex(state, &globalVisitedNodeCount, &globalVisitedNodes);
	if (!isVisited(state)) {
		addToVisited(state, &globalVisitedNodeCount, &globalVisitedNodes, &globalVisitedNodeHeuristics);
	}
	else {
		setHeuristic(state, minCost, &globalVisitedNodeHeuristics);
	}
	state->arrayIndex = indexCache;

	// Update hL
	setHeuristic(state, secondMinCost, visitedNodeHeuristics);

	if (bestChild != NULL) {
		*newState = bestChild;
	}
}

short isGoal(int frame[N][N], int goal[N][N])
{
	int i, j;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (frame[i][j] != goal[i][j]) {
				return 0;
			}
		}
	}
	return 1;
}