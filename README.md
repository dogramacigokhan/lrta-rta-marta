#### LRTA, RTA and MARTA

Learning Real-Time A*, Real-Time A* and Multi-Agent Real-Time A* implementations in C to solve [8-puzzle problem](https://en.wikipedia.org/wiki/15_puzzle).

For reference, please see related paper: Yasuhiko Kitamura, Ken-ichi Teranishi, and Shoji Tatsumi. Organizational Strategies for Multiagent Real-time Search. Proc. International Conference on Multi-Agent Systems (ICMAS-96), 150-156, 1996

#### Building the Source Code
* Run “make” in source directory, executable “prog” file should be created.
* Run executable “prog” file:
	./prog input_file
* input_file: [Required] Input file which contains the information for initialization
#### Source code is tested on:
* Ubuntu 16.04 LTS, GCC 4.8.4, c99 Standard. c99+ is required to compile the source code.