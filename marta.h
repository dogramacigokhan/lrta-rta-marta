#ifndef MARTA_H
#define MARTA_H

#include "constants.h"
#include "state.h"

enum Algorithm {
	LRTA,
	RTA,
	MARTA
};

void solve(int from[N][N], int to[N][N], int agentCount, enum Algorithm algorithm);

void lrta(State *state, int to[N][N]);
void rta(State *state, int to[N][N]);
int rta_f(State *state, int to[N][N], int *visitedNodeCount,
	int(*visitedNodes)[CACHE_SIZE][N][N], int(*visitedNodeHeuristics)[CACHE_SIZE]);
void marta(int from[N][N], int to[N][N], int agentCount);
void marta_search(State *state, int to[N][N], short *goal, int *visitedNodeCount,
	int(*visitedNodes)[CACHE_SIZE][N][N], int(*visitedNodeHeuristics)[CACHE_SIZE], State **newState);

short isGoal(int frame[N][N], int goal[N][N]);

#endif