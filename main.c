#include <stdlib.h>
#include <stdio.h>
#include "marta.h"

void readArr(FILE *fp, int arr[N][N])
{
	int i, j;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			fscanf(fp, "%d", &arr[i][j]);
		}
	}
}

int main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("Please provide the input file path.");
		exit(EXIT_FAILURE);
	}

	FILE * fp = fopen(argv[1], "r");
	if (fp == NULL) {
		printf("Unable to open input file: %s", argv[1]);
		exit(EXIT_FAILURE);
	}

	int agentCount;
	int from[N][N];
	int to[N][N];

	fscanf(fp, "%d", &agentCount);
	readArr(fp, from);
	readArr(fp, to);

	solve(from, to, agentCount, MARTA);

	printf("Press Any Key to Continue\n");
	getchar();
	return EXIT_SUCCESS;
}