﻿#include <stdio.h>
#include "state.h"

int globalVisitedNodeCount = 0;

short isVisited(State *state)
{
	return state && state->arrayIndex >= 0;
}

void setVisitedArrayIndex(State* state, int *visitedNodeCount, int (*visitedNodes)[CACHE_SIZE][N][N])
{
	// Try to set index by searching the frame in visited states cache
	int i, j, k, rowMatch;
	for (k = 0, rowMatch = 0; k < (*visitedNodeCount) - 1; k++) {
		rowMatch = k;
		for (i = 0; i < N && rowMatch >= 0; i++) {
			for (j = 0; j < N && rowMatch >= 0; j++) {
				if ((*visitedNodes)[k][i][j] != state->frame[i][j]) {
					rowMatch = -1;
				}
			}
		}
		if (rowMatch >= 0) {
			state->arrayIndex = rowMatch;
			return;
		}
	}
	state->arrayIndex = -1;
}

void addToVisited(State *state, int *visitedNodeCount,
	int (*visitedNodes)[CACHE_SIZE][N][N], int (*visitedNodeHeuristics)[CACHE_SIZE])
{
	if (isVisited(state)) {
		return;
	}

	// Add state to visited states cache
	(*visitedNodeCount)++;
	int i, j;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			(*visitedNodes)[(*visitedNodeCount) - 1][i][j] = state->frame[i][j];
		}
	}
	(*visitedNodeHeuristics)[(*visitedNodeCount) - 1] = state->heuristic;
	state->arrayIndex = (*visitedNodeCount) - 1;
}

void initState(State *state, int frame[N][N], int to[N][N], State* parent, int *visitedNodeCount,
	int (*visitedNodes)[CACHE_SIZE][N][N], int (*visitedNodeHeuristics)[CACHE_SIZE])
{
	int i;
	for (i = 0; i < N; i++) {
		memcpy(state->frame[i], &frame[i], sizeof(frame[0]));
	}
	state->childCount = 0;
	state->parent = parent;
	state->arrayIndex = -1;

	setVisitedArrayIndex(state, visitedNodeCount, visitedNodes);
	if (isVisited(state)) {
		state->heuristic = (*visitedNodeHeuristics)[state->arrayIndex];
	}
	else {
		int heuristic = calculateHeuristic(state, to);
		setHeuristic(state, heuristic, visitedNodeHeuristics);
	}
}

void setChildren(State *state, int to[N][N], int *visitedNodeCount,
	int (*visitedNodes)[CACHE_SIZE][N][N], int (*visitedNodeHeuristics)[CACHE_SIZE])
{
	const int neighbors[4][2] = {
		{-1, 0},
		{0, 1},
		{1, 0},
		{0, -1},
	};

	int zeroIndex[2];
	findZeroIndex(state->frame, zeroIndex);

	state->childCount = 0;
	int i;
	for (i = 0; i < 4; i++) {
		// Get all neighbor indices
		int neighbor[2];
		memcpy(neighbor, neighbors[i], sizeof(neighbors[0]));

		if (isInBounds(zeroIndex, neighbor)) {
			// If neighbor is in bounds, set it as a child
			int newFrame[N][N];
			swap(state->frame, zeroIndex, neighbor, newFrame);
			if (state->parent && isEqual(state->parent->frame, newFrame)) {
				continue; // Don't add parent state as a child
			}
			State *childState = malloc(sizeof(State));
			initState(childState, newFrame, to, state, visitedNodeCount, visitedNodes, visitedNodeHeuristics);
			childState->direction = getDirection(i);
			state->children[state->childCount++] = childState;
		}
	}
}

char getDirection(int index)
{
	if (index == 0) {
		return 'U';
	}
	if (index == 1) {
		return 'R';
	}
	if (index == 2) {
		return 'D';
	}
	return 'L';
}

int isEqual(int a[N][N], int b[N][N])
{
	int i, j;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (a[i][j] != b[i][j]) {
				return 0;
			}
		}
	}
	return 1;
}

void findZeroIndex(int frame[N][N], int zeroIndex[2])
{
	int i, j;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (frame[i][j] == 0) {
				zeroIndex[0] = i;
				zeroIndex[1] = j;
				return;
			}
		}
	}
}

short isInBounds(int zeroIndex[2], int neighbor[2])
{
	int i = zeroIndex[0] + neighbor[0];
	int j = zeroIndex[1] + neighbor[1];
	if (i < 0 || i >= N) {
		return 0;
	}
	if (j < 0 || j >= N) {
		return 0;
	}
	return 1;
}

void swap(int frame[N][N], int from[2], int to[2], int newFrame[N][N])
{
	int i;
	for (i = 0; i < N; i++) {
		memcpy(&newFrame[i], &frame[i], sizeof(frame[0]));
	}
	int from_i = from[0], from_j = from[1];
	int to_i = from_i + to[0], to_j = from_j + to[1];

	newFrame[to_i][to_j] = frame[from_i][from_j];
	newFrame[from_i][from_j] = frame[to_i][to_j];
}

int calculateHeuristic(State *state, int to[N][N])
{
	// Admissible heuristic "sum of Manhattan distances of misplaced tiles" is used
	if (!state) {
		return 0;
	}
	int score = 0;
	int i, j, m, n;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (state->frame[i][j] != 0) {
				findIndex(to, state->frame[i][j], &m, &n);
				score += abs(i - m) + abs(j - n);
			}
		}
	}
	return score;
}

void setHeuristic(State* state, int value, int (*visitedNodeHeuristics)[CACHE_SIZE])
{
	state->heuristic = value;
	if (isVisited(state)) {
		(*visitedNodeHeuristics)[state->arrayIndex] = value;
	}
}

void findIndex(int frame[N][N], int value, int *i, int *j)
{
	int m, n;
	for (m = 0; m < N; m++) {
		for (n = 0; n < N; n++) {
			if (frame[m][n] == value) {
				*i = m;
				*j = n;
				return;
			}
		}
	}
}

void print(State state)
{
	int i, j;
	printf("[");
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			printf("%d%s", state.frame[i][j], i == N - 1 && j == N - 1 ? "" : " ");
		}
	}
	printf("] h: %d\n", state.heuristic);
}

void freeState(State *state)
{
	if (state) {
		int i;
		for (i = 0; i < state->childCount; i++) {
			freeState(state->children[i]);
		}
		free(state);
	}
}