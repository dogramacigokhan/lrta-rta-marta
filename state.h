﻿#ifndef STATE_H
#define STATE_H

#include <stdlib.h>
#include <string.h>
#include "constants.h"

typedef struct State
{
	int frame[N][N];
	int heuristic;

	struct State *parent;
	struct State *children[4];
	int childCount;
	int arrayIndex;

	char direction;
} State;

int globalVisitedNodeCount;
int globalVisitedNodes[CACHE_SIZE][N][N];
int globalVisitedNodeHeuristics[CACHE_SIZE];

short isVisited(State *state);
void setVisitedArrayIndex(State* state, int *visitedNodeCount, int (*visitedNodes)[CACHE_SIZE][N][N]);
void addToVisited(State* state, int *visitedNodeCount,
	int (*visitedNodes)[CACHE_SIZE][N][N], int (*visitedNodeHeuristics)[CACHE_SIZE]);

void initState(State *state, int frame[N][N], int to[N][N],	State* parent, 
	int *visitedNodeCount, int (*visitedNodes)[CACHE_SIZE][N][N], int (*visitedNodeHeuristics)[CACHE_SIZE]);
void setChildren(State *state, int to[N][N], int *visitedNodeCount, 
	int (*visitedNodes)[CACHE_SIZE][N][N], int (*visitedNodeHeuristics)[CACHE_SIZE]);

char getDirection(int index);
int isEqual(int a[N][N], int b[N][N]);
void findZeroIndex(int frame[N][N], int zeroIndex[2]);
short isInBounds(int zeroIndex[2], int neighbor[2]);
void swap(int frame[N][N], int from[2], int to[2], int newFrame[N][N]);

int calculateHeuristic(State *state, int to[N][N]);
void setHeuristic(State *state, int value, int (*visitedNodeHeuristics)[CACHE_SIZE]);
void findIndex(int frame[N][N], int value, int *i, int *j);

void print(State state);
void freeState(State *state);

#endif